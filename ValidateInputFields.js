const isValid = (_this) => {
	let valid = true;
	let isAllValid4 = true;
	let phoneInputs = [..._this.template.querySelectorAll(".phone-input")];
	phoneInputs.forEach(input => {
		if (input.value) {
			console.log("input.value", input.value);
			let formattedPhone = deFormatPhoneNumber(input.value);
			console.log('formattedPhone', formattedPhone);
			let pattern = new RegExp("^(?!0{10})");
			let result = pattern.test(formattedPhone);
			console.log('result', result);
			console.log('isAllValid4', isAllValid4);
			if (result === false) {
				input.setCustomValidity("Please enter valid phone number.");
				isAllValid4 = false;
			} else {
				input.setCustomValidity(""); // if there was a custom error before, reset it
			}
			console.log('isAllValid4', isAllValid4);
		}
	});


	let isAllValid = [
		..._this.template.querySelectorAll("lightning-input")
	].reduce((validSoFar, input) => {
		if (input && input.value) input.value = input.value.trim();
		input.reportValidity();
		return validSoFar && input.checkValidity();
	}, true);

	let isAllValid1 = [
		..._this.template.querySelectorAll("lightning-combobox")
	].reduce((validSoFar, input) => {
		input.reportValidity();
		return validSoFar && input.checkValidity();
	}, true);

	let isAllValid2 = [
		..._this.template.querySelectorAll("lightning-radio-group")
	].reduce((validSoFar, input) => {
		input.reportValidity();
		return validSoFar && input.checkValidity();
	}, true);


	let isAllValid3 = [
		..._this.template.querySelectorAll("lightning-checkbox-group")
	].reduce((validSoFar, input) => {
		input.reportValidity();
		return validSoFar && input.checkValidity();
	}, true);

	console.log('All Valid', isAllValid, "isAllValid1", isAllValid1, "isAllValid2", isAllValid2, "isAllValid3", isAllValid3, "isAllValid4", isAllValid4);
	valid = isAllValid && isAllValid1 && isAllValid2 && isAllValid3 && isAllValid4;
	console.log('valid', valid);
	return valid;
}

export {
  isValid
}